
# introduce

使用 fastapi + tortoise-orm + mysql 实现一个简单的 todolist 案例

## FastAPI: 是一个现代、快速（高性能）的 Web 框架，用于基于标准 Python 类型提示使用 Python 3.6+ 构建 API。

## 官网：https://fastapi.tiangolo.com/zh/

## tortoise-orm 是一个强大的 异步 ORM 框架，用于构建高性能的数据库模型。

## 官网：https://tortoise-orm.readthedocs.io/en/latest/examples/fastapi.html

# install

```
pip3 install -r requirements.txt 

```

# run

```
python3 main.py
# or
uvicorn main:app --reload 
```

# docs

根据代码自动生成交互式文档

```
http://127.0.0.1:8000/docs
```

# Docker

定制镜像
```
docker build -t ImageName .
```
运行
```
docker run -itd -p port:port ImageName
```
